# Framework

El Framework está organizado en componentes.

Los componentes más importantes son:

* ```Bootstrap```: es el punto de entrada a la aplicación. Recibe una ```Request``` HTTP y devuelve una ```Response```. Se encarga de llamar al método ```dispatch``` del dispatcher.
* ```Router```: Se encarga de cargar las rutas definidas en un fichero ```.yml```. Este fichero se epecifica al construir el ```Router```. El ```Dispatcher``` colabora con el router para __matchear__ la ruta según el método HTTP y la URI y los posibles parámetros de ésta. Cuando encuentra una ruta válida, instancia el controlador correspondiente y llama al método del controlador especificado pasándole la ```Request```. Desde a request podremos acceder a los parámetros de la URL mediate ```Request::getUrlParams()``` Ejemplo de fichero de rutas:

```yaml
---
routes:
  -
    route: /{name}/
    http_method: GET
    controller: App\Index\Controller\Index
    controller_method: urlParams
```
* ```Di```: se encarga de resolver servicios segun un fichero de configuración. Este fichero se especifica al construir el ```Di```. Posteriormente se pueden añadir servicios mediante ```Di::set```. En el fichero de configuración de servicios se pueden espicifcar las dependencias de éstos, tanto de otros servicios (nombre precedido por una ```@```) como de tipos de datos primitivos. También permite la definición de singletos como servicios, devolviendo la misma instancia de la clase cada vez que se hace un ```Di::get``` o llamando al método __singleton__ si así se especifica. Por ejemplo, especificando ```singleton:``` en la definición del servicio, solo lo instanciará una vez. Especificando ```singleton: getInstance``` llamará al método estático ```getInstance``` de la clase cada vez que se pida ese servicio. También disponse de un método ```getByTag```, que devolverá todos aquellos servicios marcados con ese tag. Otros métodos tales como ```getFromEvent``` o ```getProfilingServices``` son métodos de utilidad basados en ```getByTag```. También se puede especificar si el servicio es público o no mediante ```public: true|false```, por defecto los servicios son públicos (si no se especifica nada). A continuación se ilustra un posible fichero de configuración del ```Di```. los servicios declarados en este fichero son obligatorios para el framework. También es obligatorio un servicio de Router, el cual es conveniente crear en la aplicación para poder especificar el fichero de configuración de rutas.

_Nota_: El componente ```Di``` no compila el fichero de dependencias, aunque sí que se guarda los servicios resueltos como callbacks para no tener que resolverlos cada vez que se piden.

```yaml
---
services:
  templateEngine:
    class: \Mpwarfrk\Component\TemplateEngine\Twig\TwigEngine
    arguments:
      - /var/www/Frameworks/Mpwarapp/app/templates
  dispatcher:
    class: \Mpwarfrk\Component\Router\Dispatcher
    arguments:
      - "@dispatcherProfiler"
  dispatcherProfiler:
    class: \Mpwarfrk\Component\Router\DispatcherProfiler
    singleton:
    tags:
      profiling:
  db:
    class: \Mpwarfrk\Component\Db\Db
    singleton:
    arguments:
    - {host: localhost, dbname: framework, user: root, password: vagrantpass}
    - "@dbProfiler"
  dbProfiler:
    class: \Mpwarfrk\Component\Db\DbProfiler
    singleton:
    tags:
      profiling:


```
* ```Response```: Este componente es devuelto por el método ```Bootstrap::run``` y contiene las cabeceras HTTP y el cuerpo de la respuesta que previamente un controlador de nuestra aplicación habrá especificado. Para enviar la respuesta se debe usar su método ```Response::send()```.
* ```BaseController``` este controller base permite acceder al motor de Templating configurado en el ```Di``` y a la base de datos a partir de las variables ```templateEngine```y ```db``` respectivamente.
* Profiling. Mediate la interfaz ```Profiler``` la qual expone el método ```Profiler::getProfilingInformationAsString``` se pueden medir los tiempos de ejecución tanto del ```Dispatcher``` como de la queries de la base de datos. Si en nuestra aplicación construimos un ```ProfilerAgreggator``` pasadole todos los servicios de profiling de la aplicación (```Di::getProfilingServices```) y después pintamos el resultado de ```ProfilerAgreggator:getProfilingInformationAsString``` podremos ver los tiempos de ejecución antes mencionados.

## Tests

Para hacer algunos tests unitarios y funcionales (muy pocos) de los componentes del framework he usado [cedeception](http://codeception.com/).

Una vez resulta la dependencia declarada en el fichero ```composer.json```, el fichero ejecutable de codeception se encuentra en ```vendor/bin/codecept```.

Para crear el directorio de tests, ejecutaremos

```codecept bootstrap <path/to/test/folder/>```

Para generar tests unitarios del tipo ```test``` ejecutaremos

```codecept generate:test unit <TestName> -c <path/to/test/folder>```

Para generar tests funcionales del tipo ```test``` ejecutaremos

```codecept generate:test functional <TestName> -c <path/to/test/folder>```

Finalmente, para ejecutar los tests ejecutaremos 

```codecept run -c <path/to/test/folder>```
