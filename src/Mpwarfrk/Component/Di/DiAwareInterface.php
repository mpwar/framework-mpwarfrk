<?php
namespace Mpwarfrk\Component\Di;


interface DiAwareInterface
{
    public function setDi(Di $di);
}