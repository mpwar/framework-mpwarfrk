<?php

namespace Mpwarfrk\Component\Di;


use Mpwarfrk\Component\Parser\Parser;

class Di
{

    const SERVICE_KEY = "@";

    private $services;

    private $resolvedServices = array();

    public function __construct($dependencies)
    {
        $this->services = $dependencies["services"];
    }

    public static function fromFile($path, Parser $parser)
    {
        $contents = file_get_contents($path);
        return self::fromFormat($contents, $parser);
    }

    public static function fromFormat($input, Parser $parser)
    {
        $parsedContents = $parser->parse($input);
        return new Di($parsedContents);
    }

    public function set($key, $instance)
    {
        $this->resolvedServices[$key] = function () use ($instance) {
            return $instance;
        };
    }

    public function get($service)
    {

        if (!isset($this->resolvedServices[$service])) {
            $this->resolvedServices = array_merge($this->resolveService($this->services, $service), $this->resolvedServices);
        }

        if (!isset($this->resolvedServices[$service])) {
            throw new \Exception("The service $service is not defined!");
        }

        if (!$this->isPublic($this->services, $service)) {
            throw new \Exception("The service $service is not public!");
        }

        $instance = $this->resolvedServices[$service]();
        if ($instance instanceof DiAwareInterface) {
            $instance->setDi($this);
        }

        return $instance;

    }

    public function getFromEvent($event)
    {
        $eventServices = array();
        foreach ($this->services as $serviceName => $serviceDefinition) {
            if ($this->isEvent($event, $this->services, $serviceName))
                $eventServices[] = $this->get($serviceName);
        }
        return $eventServices;
    }

    public function getProfilingServices()
    {
        $eventServices = array();
        foreach ($this->services as $serviceName => $serviceDefinition) {
            if ($this->isProfiling($this->services, $serviceName)) {
                $eventServices[] = $this->get($serviceName);
            }
        }
        return $eventServices;
    }

    public function getByTag($tag)
    {
        $eventServices = array();
        foreach ($this->services as $serviceName => $serviceDefinition) {
            if ($this->hasTag($tag, $this->services, $serviceName))
                $eventServices[] = $this->get($serviceName);
        }
        return $eventServices;
    }

    private function resolveService($services, $service)
    {
        $serviceDefinition = $services[$service];
        $serviceClass = $serviceDefinition["class"];
        $serviceArguments = isset($serviceDefinition["arguments"]) ? $serviceDefinition["arguments"] : [];

        if (count($serviceArguments) == 0) {
            $lazyInstance = $this->createLazyInstance($services, $service, $serviceClass);
            return ["$service" => $lazyInstance];
        }

        foreach ($serviceArguments as $argument) {
            if ($this->isArgumentAService($argument)) {
                $serviceArg = $this->removeDependencyIdentifierFromService($argument);
                if (!array_key_exists($serviceArg, $services)) {
                    $servicesAsString = implode(",", array_keys($services));
                    throw new \Exception("Service $serviceArg not found in $servicesAsString!");
                }
            }
        }

        foreach ($serviceArguments as $argument) {
            if ($this->isArgumentAService($argument)) {
                $recursiveCallResult = $this->resolveService($services, $this->removeDependencyIdentifierFromService($argument));
                $this->resolvedServices = array_merge($this->resolvedServices, $recursiveCallResult);
            }

        }

        $lazyInstance = $this->createLazyInstance($services, $service, $serviceClass, $serviceArguments);
        return ["$service" => $lazyInstance];
    }

    private function createArgumentsForInstance($serviceArguments, $resolvedServices)
    {
        $instanceArguments = array();

        foreach ($serviceArguments as $argument) {
            if ($this->isArgumentAService($argument)) {
                $instanceArguments[] = $resolvedServices[$this->removeDependencyIdentifierFromService($argument)]();
            } else {
                $instanceArguments[] = $argument;
            }
        }

        return $instanceArguments;
    }

    private function createLazyInstance($services, $service, $serviceClass, $instanceArguments = array())
    {
        $isServiceASingleton = $this->isSingleton($services, $service);
        if ($isServiceASingleton) {
            $singletonMethod = $this->getSingletonMethod($services, $service);
            if ($singletonMethod == null) {
                $instanceFactory = $this->createInstance($serviceClass, $instanceArguments);
            } else {
                $instanceFactory = $this->getInstanceFromSingleton($serviceClass, $singletonMethod);
            }
        } else {
            $instanceFactory = $this->createInstanceFactory($serviceClass, $instanceArguments);
        }
        return $instanceFactory;

    }

    private function createInstanceFactory($class, $args = array())
    {
        $resolvedServices = $this->resolvedServices;
        return (function () use ($class, $args, $resolvedServices) {
            $instanceArgs = $this->createArgumentsForInstance($args, $resolvedServices);
            return (new \ReflectionClass($class))->newInstanceArgs($instanceArgs);
        });
    }

    private function createInstance($class, $args = array())
    {
        $resolvedServices = $this->resolvedServices;

        $instanceArguments = $this->createArgumentsForInstance($args, $resolvedServices);
        $instance = (new \ReflectionClass($class))->newInstanceArgs($instanceArguments);
        return (function () use ($instance) {
            return $instance;
        });
    }

    private function getInstanceFromSingleton($class, $method)
    {
        return (function () use ($class, $method) {
            return forward_static_call_array(array($class, $method), array());
        });
    }

    private function isArgumentAService($serviceName)
    {
        if (is_array($serviceName)) return false;

        $serviceNameStart = substr($serviceName, 0, 1);
        return $serviceNameStart == self::SERVICE_KEY;
    }

    private function removeDependencyIdentifierFromService($serviceName)
    {
        return substr($serviceName, 1);
    }

    /**
     * Checks if a given service is public. Services are public by default.
     * @param $services
     * @param $service
     * @return bool
     */
    private function isPublic($services, $service)
    {
        if (!isset($services[$service]["public"])) return true;
        return $services[$service]["public"] == true;
    }

    private function hasTag($tag, $services, $service)
    {
        if (!isset($services[$service]["tags"])) return false;
        return array_key_exists($tag, $services[$service]["tags"]);
    }

    private function isProfiling($services, $service)
    {
        return $this->hasTag("profiling", $services, $service);
    }

    /**
     * Checks if a given service is defined for a specific event. False by default
     * @param $event
     * @param $services
     * @param $service
     * @return bool
     */
    private function isEvent($event, $services, $service)
    {
        if (!isset($services[$service]["tags"]["event"])) return false;
        return in_array($event, $services[$service]["tags"]["event"]);
    }

    private function isSingleton($services, $service)
    {
        if (array_key_exists("singleton", $services[$service])) {
            return true;
        }
        return false;
    }

    private function getSingletonMethod($services, $service)
    {
        $isServiceASingleton = $this->isSingleton($services, $service);
        if (!$isServiceASingleton) {
            return null;
        }
        return $services[$service]["singleton"];
    }

}