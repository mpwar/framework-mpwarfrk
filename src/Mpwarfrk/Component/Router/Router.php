<?php

namespace Mpwarfrk\Component\Router;



use Mpwarfrk\Component\Parser\Parser;
use Mpwarfrk\Component\Router\Parser\RouteValidator;

class Router
{

    private $routes;

    public function __construct()
    {
        $this->routes = [];
    }

    public function addRoute(Route $route)
    {
        $this->routes[] = $route;
    }

    public function getRoutes() {
        return $this->routes;
    }

    public static function fromFile($path, Parser $parser)
    {

        $fileContents = file_get_contents($path);
        return self::fromFormat($fileContents, $parser);

    }

    public static function fromFormat($input, Parser $parser)
    {
        $parsedArray = $parser->parse($input);
        $routes = $parsedArray[RouteValidator::ROUTES_KEY];
        $router = new Router();

        foreach ($routes as $route) {
            $router->addRoute(Route::fromArray($route));
        }
        return $router;
    }

}