<?php

require(__DIR__ . "/../../../../../../../vendor/autoload.php");

class RouteTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testRouteMatchingWithoutUrlParameters()
    {
        $this->tester->wantToTest("if a route without url params is matched");
        $r = new \Mpwarfrk\Component\Router\Route("/id/5/category/2", "GET", "None", "None");
        $this->assertTrue($r->matchRoute("/id/5/category/2") === array());
    }

    public function testRouteMatchingWithUrlParameters()
    {
        $r = new \Mpwarfrk\Component\Router\Route("/id/{id}/category/{category}", "GET", "None", "None");
        $this->assertTrue(
            $r->matchRoute("/id/5/category/2") === ["id" => "5", "category" => "2"]
            , "The route is matched and the url parameters can be extracted"
        );
        $this->assertFalse($r->matchRoute("/id2/5/category2/2"), "A non matching route is not matched");
    }
}