<?php

namespace Mpwarfrk\Component\Router\Parser;


use Mpwarfrk\Component\Router\Exception\FormatException;
use Mpwarfrk\Component\Validator\Validator;

class RouteValidator implements Validator
{

    const ROUTES_KEY = "routes";

    const URI_KEY = "route";

    const HTTP_METHOD_KEY = "http_method";

    const CONTROLLER_KEY = "controller";

    const CONTROLLER_METHOD_KEY = "controller_method";

    private $availableHttpMethods = ["GET", "POST", "PUT, PATCH", "OPTIONS", "HEAD"];

    public function validate($input) {
        $routes = $input[RouteValidator::ROUTES_KEY];


        if (!isset($routes)) {
            throw new FormatException;
        }

        foreach ($routes as $route) {
            $routeUri = $route[RouteValidator::URI_KEY];
            $routeController = $route[RouteValidator::CONTROLLER_KEY];
            $routeControllerMethod = $route[RouteValidator::CONTROLLER_METHOD_KEY];
            $routeHttpMethod = $route[RouteValidator::HTTP_METHOD_KEY];
            if (
                !isset($routeUri) ||
                !isset($routeController) ||
                !isset($routeControllerMethod) ||
                !in_array($routeHttpMethod, $this->availableHttpMethods)
            ) {
                throw new FormatException();
            }
        }
        return $input;
    }
}