<?php

namespace Mpwarfrk\Component\Router;


use Mpwarfrk\Component\Profiler\TimeProfiler;

class DispatcherProfiler extends TimeProfiler
{

    private $controller;

    private $method;

    public function __construct()
    {
        parent::__construct("Time for dispatcher to match the route");
    }


    public function setController($controller)
    {
        $this->controller = $controller;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function getProfilingInformationAsString()
    {
        $matchedController = "<br>Matched controller: {$this->controller}:{$this->method}<br>";
        return "DispatcherProfiler<br>".$matchedController.parent::getProfilingInformationAsString();
    }
}