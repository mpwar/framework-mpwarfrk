<?php

namespace Mpwarfrk\Component\Router;

use Mpwarfrk\Component\Router\Parser\RouteValidator;

class Route
{

    private $route;

    private $httpMethod;

    private $controller;

    private $controllerMethod;

    public function __construct($route, $httpMethod, $controller, $controllerMethod)
    {
        $this->route = $route;
        $this->httpMethod = $httpMethod;
        $this->controller = $controller;
        $this->controllerMethod = $controllerMethod;
    }

    public static function fromArray(array $route)
    {
        $routeUri = $route[RouteValidator::URI_KEY];
        $routeHttpMethod = $route[RouteValidator::HTTP_METHOD_KEY];
        $routeController = $route[RouteValidator::CONTROLLER_KEY];
        $routeControllerMethod = $route[RouteValidator::CONTROLLER_METHOD_KEY];
        return new Route($routeUri, $routeHttpMethod, $routeController, $routeControllerMethod);
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getControllerMethod()
    {
        return $this->controllerMethod;
    }

    public function matchRoute($route)
    {

        $params = array();

        $regexUri = $this->getRoute();

        preg_match_all('\'' . '{(\w+)}' . '\'', $regexUri, $matches);
        $matches = $matches[0];

        foreach ($matches as $key => $value) {
            $matches[$key] = str_replace('{', '', $matches[$key]);
            $matches[$key] = str_replace('}', '', $matches[$key]);
        }

        //Replace parameter names to transform URL to regex format.
        $regexUri = preg_replace('%' . '{(\w+)}' . '%', '(\w+|\d+)', $regexUri);
        $regexUri .= '$';
        $regexUri = '%^' . $regexUri . '$%';
        $res = preg_match($regexUri, $route, $params);

        if (!$res || $res == 0) {
            return false;
        }

        $paramLength = count($matches);

        $keyParams = array();

        for($i = 0; $i < $paramLength; $i++) {
            $keyParams[$matches[$i]] = $params[$i + 1];
        }

        return $keyParams;
    }

}