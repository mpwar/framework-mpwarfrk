<?php

namespace Mpwarfrk\Component\Router;


use Mpwarfrk\Component\Di\Di;
use Mpwarfrk\Component\Di\DiAwareInterface;
use Mpwarfrk\Component\Http\Request\Request;
use Mpwarfrk\Component\Http\Response\Response;
use Mpwarfrk\Component\Router\Exception\NoMatchingRouteException;

class Dispatcher implements DiAwareInterface
{

    private $profiler;

    private $di;

    public function __construct(DispatcherProfiler $profiler)
    {
        $this->profiler = $profiler;
    }

    public function dispatch(Router $router, Request $request) {

        $this->profiler->startTime();

        $triedRoutes = array();

        $requestMethod = $request->getMethod();
        $routes = $router->getRoutes();
        $requestUri = $request->getUri();

        foreach($routes as $route) {

            $triedRoutes[] = $route->getRoute();
            $params = $route->matchRoute($requestUri);
            $matches = is_array($params) && $requestMethod == $route->getHttpMethod();

            if ($matches) {
                $request->setUrlParams($params);
                $controllerName = $route->getController();
                $controllerMethod = $route->getControllerMethod();
                $controller = (new \ReflectionClass($controllerName))->newInstance();
                if ($controller instanceof DiAwareInterface) {
                    $controller->setDi($this->di);
                }
                $response = call_user_func_array(array($controller, $controllerMethod), array($request));
                $this->profiler->setController($controllerName);
                $this->profiler->setMethod($controllerMethod);
                if (is_string($response)) {
                    $this->profiler->endTime();
                    return new Response($response);
                }
                $this->profiler->endTime();
                return $response;
            }
        }

        $triedRoutesString = implode(", ", $triedRoutes);
        throw new NoMatchingRouteException(
            "The route {$request->getUri()} could not be matched, tried $triedRoutesString"
        );
    }

    public function setDi(Di $di)
    {
        $this->di = $di;
    }
}