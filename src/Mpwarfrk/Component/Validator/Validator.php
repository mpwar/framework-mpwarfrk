<?php

namespace Mpwarfrk\Component\Validator;


interface Validator
{
    public function validate($input);
}