<?php

namespace Mpwarfrk\Component\Bootstrap;
use Mpwarfrk\Component\Di\Di;
use Mpwarfrk\Component\Http\Request\Request;

class Bootstrap
{

    private $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function run(Request $request) {
        $dispatcher = $this->di->get("dispatcher");
        $router = $this->di->get("router");
        return $dispatcher->dispatch($router, $request);
    }

}