<?php

namespace Mpwarfrk\Component\Parser;

use Mpwarfrk\Component\Validator\Validator;
use Symfony\Component\Yaml\Yaml;


class YamlParser implements Parser
{

    public function parse($input, Validator $validator = null)
    {
        $contents = Yaml::parse($input);
        if ($validator != null) {
            $contents = $validator->validate($contents);
        }
        return $contents;
    }
}