<?php

namespace Mpwarfrk\Component\Parser;

use Mpwarfrk\Component\Validator\Validator;

interface Parser
{
    public function parse($input, Validator $validator = null);
}