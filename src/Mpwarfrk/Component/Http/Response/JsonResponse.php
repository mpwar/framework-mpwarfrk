<?php

namespace Mpwarfrk\Component\Http\Response;


class JsonResponse extends Response
{

    public function __construct($body, $status = 200, array $headers = array())
    {
        parent::__construct($body, $status, $headers);
        $this->addHeader(["Content-Type" => "application/json"]);
    }


    protected function getBody()
    {
        if (is_string($this->body)) return $this->body;
        return json_encode($this->body);
    }
}