<?php

namespace Mpwarfrk\Component\Http\Response;


use Mpwarfrk\Component\Http\Codes;

class Response
{

    protected $body;

    protected $headers;

    protected $status;

    public function __construct($body, $status = 200, $headers = array())
    {
        $this->body = $body;
        $this->status = $status;
        $this->headers = $headers;
    }

    public function setBody($body) {
        $this->body = $body;
    }

    public function setHeaders(array $headers) {
        $this->headers = $headers;
    }

    public function addHeader($header) {
        $this->headers = array_merge($this->headers, $header);
    }

    public function getHeaders() {
        return $this->headers;
    }

    public function getHeader($key) {
        return $this->headers[$key];
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function toString()
    {
        $response =
            $this->generateStatus()
            . $this->generateHeaders()
            . "\r\n"
            . $this->getBody()
            . "\r\n";
        return $response;
    }

    public function send() {
        header($this->generateStatus());
        foreach ($this->headers as $key => $value) {
            header("$key: $value");
        }
        echo "\r\n";
        echo $this->getBody();
        echo "\r\n";
    }

    protected function generateHeaders()
    {
        $headerString = "";
        $headers = $this->headers;
        foreach ($headers as $key => $value) {
            $headerString .= "$key: $value\r\n";
        }
        return $headerString;
    }

    protected function generateStatus()
    {
        $code = Codes::HTTP_CODES[$this->status];
        return "HTTP/1.1 {$this->status} {$code}\r\n";
    }

    protected function getBody() {
        return $this->body;
    }
}