<?php

namespace Mpwarfrk\Component\Http\Request;


class Request
{

    private $server;

    private $query;

    private $request;

    private $headers;

    private $file;

    private $cookies;

    private $body;

    private $session;

    private $urlParams = array();

    public function __construct($server = array(), $query = array(), $request = array(), $headers = array(), $session = array(), $cookies = array(), $body = array(), $file = array())
    {
        $this->server = $server;
        $this->query = $query;
        $this->request = $request;
        $this->headers = $headers;
        $this->file = $file;
        $this->cookies = $cookies;
        $this->body = $body;
        $this->session = $session;
    }


    public function getUri()
    {
        return $this->server["REQUEST_URI"];
    }

    public function getMethod()
    {
        return $this->server["REQUEST_METHOD"];
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setUrlParams(array $urlParams) {
        $this->urlParams = $urlParams;
    }

    public function getUrlParam($key) {
        return $this->urlParams[$key];
    }

    public function getQuery($key)
    {
        return isset($this->query[$key]) ? $this->query[$key] : null;
    }

    public function getRequest($key)
    {
        return isset($this->request[$key]) ? $this->request[$key] : null;
    }

    public function getSession($key)
    {
        return isset($this->session[$key]) ? $this->session[$key] : null;
    }

    public function getCookie($key)
    {
        return isset($this->cookies[$key]) ? $this->cookies[$key] : null;
    }
}