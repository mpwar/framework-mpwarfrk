<?php

namespace Mpwarfrk\Component\Db;


class Db
{
    /**
     * @var \PDO
     */
    protected $connection;

    protected $profiler;

    public function __construct($config, DbProfiler $profiler)
    {
        $this->connection = new \PDO("mysql:host=${config["host"]};dbname=${config["dbname"]}", $config["user"], $config["password"]);
        $this->profiler = $profiler;
    }

    public function execute($selector)
    {
        $queryProfiler = $this->profiler->newQuery($selector);
        $queryProfiler->startTime();
        $stmt = $this->connection->query($selector);
        $queryProfiler->endTime();
        return $stmt;
    }

    public function executeQuery($selector)
    {
        $queryProfiler = $this->profiler->newQuery($selector);
        $queryProfiler->startTime();
        $stmt = $this->connection->query($selector);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        $queryProfiler->endTime();
        return $result;
    }

}