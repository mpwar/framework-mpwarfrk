<?php

namespace Mpwarfrk\Component\Db;


use Mpwarfrk\Component\Profiler\Profiler;

class DbProfiler implements Profiler
{

    private $queries = array();

    public function newQuery($identifier)
    {
        $query = new QueryProfiler($identifier);
        $this->queries[] = $query;
        return $query;
    }

    public function getProfilingInformationAsString()
    {
        $data = "<br>DbProfiler";
        foreach ($this->queries as $query) {
            $data .= "{$query->getProfilingInformationAsString()}";
        }
        return $data;
    }
}