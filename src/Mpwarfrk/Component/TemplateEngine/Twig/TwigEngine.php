<?php

namespace Mpwarfrk\Component\TemplateEngine\Twig;

use Mpwarfrk\Component\TemplateEngine\TemplateEngine;

class TwigEngine implements TemplateEngine
{

    private $backend;

    public function __construct($pathToTemplates) {
        $loader = new \Twig_Loader_Filesystem($pathToTemplates);
        $this->backend = new \Twig_Environment($loader);
    }

    public function render($template, $params = null)
    {
        if (is_null($params)) return $this->backend->render($template);
        return $this->backend->render($template, $params);
    }
}