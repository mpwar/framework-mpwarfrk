<?php

date_default_timezone_set('Europe/Madrid');

class SmartyTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSmartyReturnsARenderedTemplate()
    {
        $te = new \Mpwarfrk\Component\TemplateEngine\Smarty\SmartyEngine(__DIR__."/../../smarty_template", __DIR__."/../../smarty_template_c");
        $res = $te->render("base.tpl", ["greeting" => "Smarty!"]);
        $this->assertTrue($res == "Hi am am using Smarty and I want to say Smarty!", "The rendered template was $res");
    }
}