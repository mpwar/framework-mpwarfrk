<?php

date_default_timezone_set('Europe/Madrid');

class TwigTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testTwigReturnsARenderedTemplate()
    {
        $te = new \Mpwarfrk\Component\TemplateEngine\Twig\TwigEngine(__DIR__."/../../twig_template");
        $res = $te->render("base.twig", ["greeting" => "Twig!"]);
        $this->assertTrue($res == "Hi am am using Twig and I want to say Twig!", "The rendered template was $res");
    }
}