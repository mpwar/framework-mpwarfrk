<?php

namespace Mpwarfrk\Component\TemplateEngine;


interface TemplateEngine
{
    public function render($template, $params = null);
}