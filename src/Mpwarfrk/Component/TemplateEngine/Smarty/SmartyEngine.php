<?php

namespace Mpwarfrk\Component\TemplateEngine\Smarty;


use Mpwarfrk\Component\TemplateEngine\TemplateEngine;

class SmartyEngine implements TemplateEngine
{

    private $backend;

    public function __construct($templatePath, $compilePath = null) {
        $this->backend = new \Smarty();
        $this->backend->setTemplateDir($templatePath);
        if (!is_null($compilePath)) $this->backend->setCompileDir($compilePath);
    }

    public function render($template, $params = null)
    {
        if (!is_null($params)) {
            foreach ($params as $key => $value) {
                $this->backend->assign($key, $value);
            }
        }
        return $this->backend->fetch($template);
    }
}