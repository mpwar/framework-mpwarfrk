<?php

namespace Mpwarfrk\Component\Controller;

use Mpwarfrk\Component\Db\Db;
use Mpwarfrk\Component\Di\Di;
use Mpwarfrk\Component\Di\DiAwareInterface;

class BaseController implements DiAwareInterface
{
    protected $templateEngine;

    /**
     * @var Db
     */
    protected $db;

    public function setDi(Di $di)
    {
        $this->templateEngine = $di->get("templateEngine");
        $this->db = $di->get("db");
    }
}