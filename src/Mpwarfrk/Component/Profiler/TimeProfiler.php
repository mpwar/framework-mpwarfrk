<?php

namespace Mpwarfrk\Component\Profiler;


class TimeProfiler implements Profiler
{

    private $identifier;

    private $startTime;

    private $endTime;

    public function __construct($identifier)
    {
        $this->identifier = $identifier;
    }

    public function startTime()
    {
        $this->startTime = microtime(true);
    }

    public function endTime()
    {
        $this->endTime = microtime(true);
    }

    public function getProfilingInformationAsString()
    {
        $diff = $this->endTime - $this->startTime;
        $sec = intval($diff);
        $micro = $diff - $sec;
        return "<br>{$this->identifier}: $micro seconds<br>";
    }
}