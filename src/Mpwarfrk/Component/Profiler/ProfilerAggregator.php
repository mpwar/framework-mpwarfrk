<?php

namespace Mpwarfrk\Component\Profiler;


class ProfilerAggregator implements Profiler
{

    private $profilingServices;

    public function __construct(array $profilingServices)
    {
        $this->profilingServices = $profilingServices;
    }

    public function getProfilingInformationAsString()
    {
        $data = "<br>";
        foreach ($this->profilingServices as $profilingService) {
            $data .= "{$profilingService->getProfilingInformationAsString()}";
        }

        return $data;
    }
}