<?php

namespace Mpwarfrk\Component\Profiler;

interface Profiler
{

    public function getProfilingInformationAsString();

}